---
title: "Kaggle Competition Writeup"
author: "Ricardo Batista (rb313)"
date: "4/18/2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(knitr)
library(DMwR)
library(caret)
library(PCAmixdata)
library(xgboost)
library(glmnet)
library(neuralnet)
library(plsdepot)

# Delete
library(tictoc)
submission <- read.csv("pml_example_submission.csv", header = TRUE)

set.seed(2018)

train_raw      <- read.csv("pml_train.csv", header = TRUE)
test_raw       <- read.csv("pml_test_features.csv", header = TRUE)

```

# Introduction


# 0 Data Cleaning

&nbsp;&nbsp;&nbsp;&nbsp; Several EDA and model selection methods require special transformations, subsets, and partitions of `pml_train`. As such, we perform the following data cleaning operations and generate ancillary data frames (or matrices):  

- remove variable `id` from `train` (since it has no predicitve power);  
- create design matrices $X_{train}$ and $X_{test}$;  
- identify and remove covariate values (from $X_{train}$ and $X_{test}$) that don't appear in both `pml_train` and `pml_test_features`;  
- separate $X_{train}$ into continuous and categorical variables; and,  
- apply a Box-Cox transformation to correct for skewness, center and scale each continuous variable (on $X_{train}$).  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Remove variable `id` from `train`
train <- dplyr::select(train_raw, -id)
test  <- dplyr::select(test_raw, -id)


# create design matrix X
X_raw      <- model.matrix(loss ~ - 1 + . , train)
X_test_raw <- model.matrix( ~ - 1 + . , test)

loss <- as.matrix(train$loss)
colnames(loss) <- c("loss")

# Identifying and removing covariate values that don't appear on both sets
cov.names_intersect <- intersect(colnames(X_raw), colnames(X_test_raw))
X      <- X_raw[ , cov.names_intersect]
X_test <- X_test_raw[ , cov.names_intersect]


# separate $X_train$ and $X_test$ into continuous and categorical variables
cont.var <- grep("cont", colnames(X), value = T)
cat.var  <- grep("cat", colnames(X), value = T)

X_cont <- X[, cont.var]
X_cat  <- X[, cat.var]
X_test_cont <- X_test[, cont.var]
X_test_cat  <- X_test[, cat.var]


# scale the data
cont        <- rbind(X_cont, X_test_cont)
cont_scaled <- scale(cont)
loss_scaled <- scale(loss)

# scale continous variables
X_std        <- cbind(X_cat, head(cont_scaled, dim(X_cont)[1]))
X_std_w.loss <- cbind(X_std, loss_scaled)
X_test_std   <- cbind(X_test_cat, tail(cont_scaled, dim(X_test_cont)[1]))

# Create factored version of X for PCA apply a Box-Cox transformation to correct for skewness, center and scale each continuous variable.
#require(caret)
# use preprecessinng for PCA preProcess(X_cont, method = c("scale", "center")) #add box cox for PCA
# X_cont_std      <- data.frame(predict(trans, X_cont))
#X_cat_fact <- as.character(X_cat)

```

# 1 Exploratory Analysis

&nbsp;&nbsp;&nbsp;&nbsp; First, we will explore linear realitonGiven the high-dimensionality, we'll perform Partial Least Squares (PLS) and Multiple Correspondence Analysis (MCA) on the continuous and categorical variables, respectively.  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

tic("PCA")
pcamix_output <- PCAmix(X.quanti = train_cont_std, X.quali = train_cat, rename.level = TRUE, graph = FALSE)
toc()

tic("PLS")
pls <- plsreg1(X_std, loss_scaled, comps = 3)
toc()


plot(pls, cex = 0.1, what = "observations")

```


&nbsp;&nbsp;&nbsp;&nbsp; However, only linear relationships are considered in PCA, so we run exploratory XGBoost to explore the potential multivariate nature of the data (e.g., higher order interaction between variables).  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

#### Boosting ####
xg_output <- xgboost(data = X, label = train$loss, nrounds = 50)

# Variable importance
importance_matrix <- xgb.importance(colnames(X), model = xg_output)
xgb.plot.importance(importance_matrix, rel_to_first = TRUE, xlab = "Relative importance")

# Variable subset (arbitrarily chose 0.5%)
importance_matrix_sub <- importance_matrix %>%
  dplyr::select(c(Feature, Importance)) %>%
  filter(Importance > 0.005)

kable(importance_matrix_sub)


```

# 2 Models

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# fit neural network

formula_m.NN <- as.formula(paste("loss ~ ", paste(importance_matrix_sub$Feature, collapse = " + "), collapse = " "))
NN <- neuralnet(formula_m.NN, data = X_std_w.loss, hidden = 42, 
                act.fct = "tanh", lifesign = "full", threshold = 40,
                linear.output = T)
m # rep = 68000, learningrate.limit = c(0.0001, 0.01), algorithm = "rprop+", learningrate = 0.01, 

### attributes(NN)$names

#### Major insight: realize that what determines convergence is the min threshhold --> you can change this at will!!

# stuff I tried:
## fewer neurons
## more layers
## fewer repetitions

# complete

### c(42), threshold = 40

### c(5, 3, 2), threshold = 40
### 16906	error: 30794.06413 --> 1315.44158

### c(10), threshold = 40
### 24482	error: 30251.05993  --> 1308.34534


### c(10), threshold = 50
### 5136	error: 31620.52286  --> 1307.34602

# 100000

### c(10), threshold = 50
## 3812	error: 24031.64873  --> 1307.78984


# 10000

### c(10), threshold = 1
### 8465	error: 2190.99328 --> 1377.33709

### c(5, 3), threshold = 2 
### 4884	error: 2220.50366 ---> 1380.95490

### c(20), threshold = 2
#### 6159	error: 2000.77985 --> 1427.05551

### c(10), threshold = 2
#### 3028	error: 2161.49987 -->  Kaggle 1383.47467

### c(20, 10, 5)
#### 27695	error: 1711.4165 --> Kaggle 1649.65944

### c(10, 5, 2)
#### 35590	error: 1884.90564  --> Kaggle 1,440

### try stepmax and looking at error produced --> does doing stepmax produce a full output??

### 100,000 (with threshold of 50)
#c(10)
#c(5,3)


### 10,000
#c(5,3)  51209	error: 2092.9178 	time: 5.73 mins
#c(10)   17007	error: 2126.7303 	time: 2.62 mins


### try rep = 68000

### https://www.researchgate.net/post/In_neural_networks_model_which_number_of_hidden_units_to_select
#### N = number of inputs, and m = number of outputs. (5, 3)
#1st hidden layer sqrt[(m+2)N] + 2sqrt[N/(m+2)]  
#2nd hidden layer m*sqrt[N/(m+2)]



### With 42
# 11000	min thresh: 3.178080518

### With 10000
#### 1000

### With 1000
#### 100  --> converged!

#### With 500   [  // output: range  // output: scale]
### 100 --> converged
### 16
### 
### c(20,20) 0.001084542051 // 
### c(10)   0.003288246686 // 0.0002370376012 // 0.0001555831256
### c(20, 10)   //   // 0.000538787512
### c(20)   //   // min thresh: 0.01179199852
### c(length(importance_matrix_sub$Feature), 10, 5)
### c(20, 20, 5)
### c(10,10) // // 0.02834417139



#### With all   [  // output: range  // output: scale]

### hidden = length(importance_matrix_sub$Feature)
### rep = 20

X_test_std_NN <- X_test_std[,importance_matrix_sub$Feature]
predictions_std <- compute(NN, X_test_std_NN)
submission$loss <- unscale(predictions_std$net.result, loss_scaled)
write.csv(submission, file = "submission.csv", row.names = FALSE)

```


# 3 Training



# 4 Hyperparameter Selection



# 5 Data Splits



# 6 Errors and Mistakes




# 7 Predictive Accuracy


