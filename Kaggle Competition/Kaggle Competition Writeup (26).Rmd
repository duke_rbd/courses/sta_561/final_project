---
title: "Kaggle Competition Writeup"
author: "Ricardo Batista (rb313)"
date: "4/18/2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(knitr)
library(caret)
library(plsdepot)
library(ggrepel)
library(xgboost)
library(neuralnet)
library(tictoc)

set.seed(2018)

train_raw  <- read.csv("pml_train.csv", header = TRUE)
test_raw   <- read.csv("pml_test_features.csv", header = TRUE)
submission <- read.csv("pml_example_submission.csv", header = TRUE)


l2.norm <- function(p_1, p_2){
  sqrt(sum((p_1 - p_2)^2))
}

dists.to.point <- function(p, set){
  dists <- apply(set, 1, l2.norm, p)
  return(dists)
}

rmse <- function(y, y_hat) {sqrt(mean((y - y_hat)^2))}

```

# Introduction


# 0 Data Cleaning

&nbsp;&nbsp;&nbsp;&nbsp; EDA and model selection methods in this analysis require special transformations, subsetting, and partitioning of the data. As such, we generate ancillary data frames by performing the following data cleaning operations:  

- remove variable `id` from `train` (since it has no predicitve power);
- create design matrices $X_{train}$ and $X_{test}$;
- identify and remove covariate values (from $X_{train}$ and $X_{test}$) that don't appear in both `pml_train` and `pml_test_features`;
- separate $X_{train}$ into continuous and categorical variables;
- scale continuous variables; and,
- apply a Box-Cox transformation to correct for skewness.  

\vspace{10pt}

```{r, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Remove variable `id` from `train`
train <- dplyr::select(train_raw, -id)
test  <- dplyr::select(test_raw, -id)


# create design matrix X
X_raw      <- model.matrix(loss ~ - 1 + . , train)
X_test_raw <- model.matrix( ~ - 1 + . , test)

loss <- as.matrix(train$loss)
colnames(loss) <- c("loss")

# Identifying and removing covariate values that don't appear on both sets
cov.names_intersect <- intersect(colnames(X_raw), colnames(X_test_raw))
X      <- X_raw[ , cov.names_intersect]
X_test <- X_test_raw[ , cov.names_intersect]


# separate $train$ into continuous and categorical variables
cont.var <- grep("cont", colnames(X), value = T)
cat.var  <- grep("cat", colnames(X), value = T)

X_cont <- X[, cont.var]
X_cat  <- X[, cat.var]
X_test_cont <- X_test[, cont.var]
X_test_cat  <- X_test[, cat.var]


# scale continous variables
cont        <- rbind(X_cont, X_test_cont)
cont_scaled <- scale(cont)
loss_scaled <- scale(loss)

X_std        <- cbind(X_cat, head(cont_scaled, dim(X_cont)[1]))
X_std_w.loss <- cbind(X_std, loss_scaled)
X_test_std   <- cbind(X_test_cat, tail(cont_scaled, dim(X_test_cont)[1]))

# apply a Box-Cox transformation to correct for skewness, center and 
# scale each continuous variable.
require(caret)
trans      <- preProcess(X_cont, method = c("BoxCox", "center", "scale"))
X_cont_pls <- data.frame(predict(trans, X_cont))
X_std_pls  <- cbind(X_cat, X_cont_pls)

```

# 1 Exploratory Analysis

&nbsp;&nbsp;&nbsp;&nbsp; First, we explore linear relationships that might exist between covariates and response. Given the high-dimensionality, we'll perform Partial Least Squares (PLS).  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4, eval = FALSE}

pls <- plsreg1(X_std_pls, loss_scaled, comps = 3)

plot(pls, cex = 0.5, what = "variables")
plot(pls, cex = 0.1, what = "observations")

```

The analysis reveals that the first three principal components (PCs) only account for $\approx 3 \%$ of the cumulative variance in the data. The "Observations" plot certainly belies this notion, given that the relatively tight distribution along the x-axis seems to suggest that PC1 accounts for more variance than it does. Keep in mind, however, that PLS only considers linear relationships, so we run exploratory XGBoost (i.e., few rounds) to study potential multivariate nature of the data (e.g., higher order interaction between variables).  

\vspace{10pt}

```{r, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4, eval = FALSE}

#### Boosting ####
xg_output <- xgboost(data = X, label = train$loss, nrounds = 1000)

# Variable importance
importance_matrix <- xgb.importance(colnames(X), model = xg_output)
xgb.plot.importance(importance_matrix, rel_to_first = TRUE, xlab = "Relative importance")

```

We focus on the variables with greatest relative significance:  

```{r, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4, eval = FALSE}

# Variable subset (arbitrarily chose 0.5%)
importance_matrix_sub <- importance_matrix %>%
  dplyr::select(c(Feature, Importance)) %>%
  filter(Importance > 0.005)

kable(importance_matrix_sub)

```

\pagebreak

# 2 Models

&nbsp;&nbsp;&nbsp;&nbsp; We believe **XGBoost** and a **neural network** are best positioned for prediction given the following:  

- *Nature of relationship between covariates and response* -- As revealed in EDA, there is no clear linear relationship between our covariates and target. Both XGBoost and neural nets are free to learn complex non-linear decision boundaries.  

- *Multicollinearity and concurvity* -- As PLS analysis revealed, there is significant multicollinearity among predictors. Moreover, given the number of covariates, we may also be dealing with concurvity -- nonlinear dependencies among the predictor variables. Recall that with decision trees, if two features are strongly correlated, one of the features is arbitrarily chosen for a split and the other feature has no further effect on the model. Similarly, due to their redundant architecture, neural nets are also robust against such dependencies [1].  

- *Additional benefits* -- Both models have high-quality libraries online: the `neuralnet`[2] and `xgboost` [3] R packages. For instance, the `xgboost` package provides a cross validation function. Both algorithms also have simple parametrization and relatively high computational efficiency.  


&nbsp;&nbsp;&nbsp;&nbsp; Not only do boosting and neural nets outperform other established model types, they complement each other well:  

- *Computational efficiency* -- Neural nets do not scale particularly well with the number of observations, hidden layers, or number of neurons. On the other hand, XGboost is notoriously fast regardless of the parametrization.  

- *Modeling flexibility* -- Neural nets can learn arbitrarily complex functions. This allows us to capture underlying hierarchical concepts and compositions in the data. However, if we're not careful, neural nets can overfit very easily, particularly if the number of observations isn't in the millions. [4]  


- *Outlier scaling* -- Decision trees don’t require any special treatment for outliers, whereas neural nets are more suceptible.  

[1] http://www.cis.upenn.edu/~ungar/Datamining/Publications/tale2.pdf  
[2] https://cran.r-project.org/web/packages/neuralnet/neuralnet.pdf  
[3] https://cran.r-project.org/web/packages/xgboost/xgboost.pdf  
[4] https://www.quora.com/Why-does-Gradient-boosting-work-so-well-for-so-many-Kaggle-problems  
[5] https://scialert.net/fulltext/?doi=jas.2005.1394.1398  


# 3 Training

## Neural Network

&nbsp;&nbsp;&nbsp;&nbsp; We tested a wide range of parameters on subsets of all orders of magnitude (less than 1e6). As such, training times ranged from a few seconds to 12 hours. Each run (i.e., each of the repetitions for each of the 10 folds) of the final algorithm (i.e., 1 hidden layer, 10 units) took 3 minutes. That is, a single run of cross-validated neural network took about 2.5 hours. We use the subset of variables with highest relative importance highlighted in the "EDA" section.  

### Overview of Neural Networks

&nbsp;&nbsp;&nbsp;&nbsp; Neural nets are a supervised learning algorithm powerful enough to model arbitrarily complex functions. It can accomplish this by reproducing certain fundamental brain structures. Neural nets are composed of an input layer, hidden layers, and output layers. Each hidden layer is composed of hidden units (i.e., artificial neurons) which "activate" based on input from lower layers and weights associated with each input. This neural network uses *resilient backpropagation* to train said weights.   

### Resilient backpropagation

&nbsp;&nbsp;&nbsp;&nbsp; All algorithms try to minimize the error function by adding a learning rate to the weights going into the opposite direction of the gradient. [6]  

\begin{figure}[!h]
    \caption{Neural network training pseudocode}
    \centering
    \includegraphics[width=0.75\textwidth]{pseudocode_NN.png}
\end{figure}

Unlike the traditional backpropagation algorithm, a separate learning rate $\eta_k$, which can be changed during the training process, is used for each weight in resilient backpropagation. This solves the problem of defining an over-all learning rate that is appropriate for the whole training process and the entire network. Additionally, instead of the magnitude of the partial derivatives only their sign is used to update the weights. This guarantees an equal influence of the learning rate over the entire network.  

&nbsp;&nbsp;&nbsp;&nbsp; In order to speed up convergence in shallow areas, the learning rate $\eta_k$ will be increased if the corresponding partial derivative keeps its sign. On the contrary, it will be decreased if the partial derivative of the error function changes its sign since a changing sign indicates that the minimum is missed due to a too large learning rate. Weight backtracking is a technique of undoing the last iteration and adding a smaller value to the weight in the next step. Without the usage of weight backtracking, the algorithm can jump over the minimum several times.

&nbsp;&nbsp;&nbsp;&nbsp; The pseudocode of resilient backpropagation with weight backtracking is given by  

```{r, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4, eval = FALSE}

# for all weights{
#   if (grad.old*grad>0){
#     delta := min(delta*eta.plus, delta.max)
#     weights := weights - sign(grad)*delta
#     grad.old := grad
#   }
#   else if (grad.old*grad<0){
#     weights := weights + sign(grad.old)*delta
#     delta := max(delta*eta.minus, delta.min)
#     grad.old := 0
#   }
#   else if (grad.old*grad=0){
#     weights := weights - sign(grad)*delta
#     grad.old := grad
#   }
# }

```

while that of the regular backpropagation is given by  

```{r, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4, eval = FALSE}
# for all weights{
#   weights := weights - grad*delta
# }
```

[6] https://journal.r-project.org/archive/2010/RJ-2010-006/RJ-2010-006.pdf



## XGboost

&nbsp;&nbsp;&nbsp;&nbsp; Similar to our neural net training, we tested a wide range of parameters on $1,000$ to $100,000$ rounds. Thus, training times ranged from a few seconds to 24 hours. Each run (i.e., each of the repetitions for each of the 10 folds) of the final algorithm (`nrounds` = $2,000$) took 5 minutes. That is, a single run of cross-validated XGboost took about 50 minutes.    

&nbsp;&nbsp;&nbsp;&nbsp; Similar to neural nets, we tested a wide range of parameters. Training times for each run of the final algorithm As such, training times ranged from a few seconds to 12 hours. Each run of the final algorithm took 5 minutes.  

### Overview of boosted trees

&nbsp;&nbsp;&nbsp;&nbsp; Unlike fitting a single large decision tree to the data, which amounts to fitting the data hard and potentially overfitting, the boosting approach instead learns slowly. Given the current model, we fit a decision tree to the residuals from the model. That is,  
- (1) we fit a tree using the current residuals, rather than the outcome Y , as the response.  
- (2) We then add this new decision tree into the fitted function in order to update the residuals. Each of these trees can be rather small, with just a few terminal nodes, determined by the parameter d in the algorithm.  
- (3) By fitting small trees to the residuals, we slowly improve ˆf in areas where it does not perform well. The shrinkage parameter $\lambda$ slows the process down even further, allowing more and different shaped trees to attack the residuals.[7]  

\begin{figure}[!h]
    \caption{Boosting pseudocode}
    \centering
    \includegraphics[width=0.75\textwidth]{pseudocode_boosting.png}
\end{figure}


### XGBoost

&nbsp;&nbsp;&nbsp;&nbsp; XGBoost uses the Newton tree boosting (NTB) algorithm for fitting additive tree models.  

\begin{figure}[!h]
    \caption{XGBoost pseudocode}
    \centering
    \includegraphics[width=0.75\textwidth]{pseudocode_XGboost.png}
\end{figure}

The general steps includes 3 stages:  

- (1) Determine the leaf weights $\texttildelow{\omega}_{jm}$ for a fixed candidate tree structure;
- (2) Different tree structures are proposed with weights determined from the previous stage, therefore, the tree structure and and the regions are determined;, and,
- (3) Once a tree structure is settled upon, the final leaf weights $\texttildelow{\omega}_{jm}, j=1,..,T$ in each terminal node are determined.  

XGBoost is renown for learning accurate tree structures. It is adept at uncovering local neighbourhoods. This stems from the fact that XGBoost tries to minimize a second-order approximation of the log-loss. In doing so, it uses the Hessian matrix, which entails a higher-order approximation of the loss function. 

&nbsp;&nbsp;&nbsp;&nbsp; XGBoost also employs three types of regularization parameter:  

(1) *boosting parameters* -- number of trees M and the learning rate $\eta$.
(2) *tree parameters* -- constraints and penalties on the complexities of individual trees. A terminal node penalization $\gamma$ might be included, thus the number of terminal nodes might vary and bounded by a maximum number of terminal nodes. XGBoost implements the $l2$ regularization on leaf weights, and will implement $L1$ regularization on leaf weights.
(3) *randomization parameters* -- row and column subsampling. XGBoost provides column subsampling in addition to row subsampling.  

Finally, we were able to use its parallelization feature (given we have OpenMP).  

[7] James, G; Witten, D; Hastie, T; Tibshirani, R. An Introduction to Statistical Learning. Springer, New York. 2013.  
[8] https://medium.com/@Synced/tree-boosting-with-xgboost-why-does-xgboost-win-every-  
machine-learning-competition-ca8034c0b283


# 4 Hyperparameter Selection

## Neural Network

- *Hidden layers and neurons per layer* -- Optimal network structure seems to be problem specific. In general, the number of hidden layers and units seems to depend on the data complexity and number of observations. Although researchers have advanced many different benchmarks, finding the optimal structure seems to rely heavily on guessing an checking. We abided by a few such popular benchmarks: the maximum number of hidden units in any layer did not exceed the number of inputs, most functions can be modeled with a single hidden layer (and no more than 3). Below is the result of such experimentation. Each line represents a different network structure, where each element in the relevant vector represents the number of hidden units per layer. The point lables represent the number of observations used to train the model. Although not easily perceptible, model accuracy did not improve with complexity. As such, we abide by Occam's razor: we prefered a single hidden layer with 10 units over the more complex structures.    

\vspace{10pt}

```{r, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4, eval = FALSE}

structure  <- c("c(10, 5, 2)", "c(20, 10, 5)", "c(10)", "c(20)", "c(5, 3)", 
                "c(10)", "c(10)", "c(10)", "c(10)", "c(5, 3, 2)")
threshold  <- c(0, 0, 2, 2, 2, 1, 50, 50, 40, 40)
MAE        <- c(1440, 1649.65944, 1383.47467, 1427.05551, 1380.95490, 
                1377.33709, 1307.78984, 1307.34602, 1308.34534, 1315.44158)
train_size <- c("1e4", "1e4", "1e4", "1e4", "1e4", "1e4", "1e5", "full", "full", "full")

NN_training <- data.frame(structure, threshold, MAE)

ggplot(NN_training, aes(threshold, MAE, label = train_size, color = structure)) + 
  geom_point() + geom_line() + 
  geom_text_repel(aes(label = train_size), hjust = -.5, vjust = -.5) +
  labs(title = "Mean Absolute Error by threshold (and training set size)", 
       x = "Threshold", y = "Mean Absolute Error")

```

\vspace{10pt}

- *Threshold* -- A numeric value specifying the threshold for the partial derivatives of the error function as stopping criteria. In other words, the model will continue to iterate and try finding the best solution until it reaches a point where the overall error of the model is not reducing by more than the given threshold value [9]. We scaled this threshold with the number of observations since reaching a given threshold takes orders of magnitude more interations with an order-of-magnitude increase in number of observations.  

- *Maximum number of steps* (i.e., stepmax) -- Reaching this maximum leads to a stop of the neural network's training process. This criteria is not enforced in practice. In any case, the algorithm took anywhere from $20,000$ to $100,000$ steps to converge. 

- *Repetitions* (i.e., `rep`) -- For each set of parameters, we ran the algorithm $5$ times and logged the average weight and training error.  

- *Algorithm* -- As alluded to in the previous section, we used resilient backpropagation with weight backtracking (i.e., `rprop+`) as our training function.  

- *Activation function* -- We chose hyperbolic tangent (i.e., `tanh`) given convergence problems associated with sigmoid-like activation functions.  



## XGboost

&nbsp;&nbsp;&nbsp;&nbsp;  

Booster Parameters

- *learning rate* (i.e., `eta`) -- Just as threshold for neural networks, lower value for eta implies larger value for nrounds: low eta value means model more robust to overfitting but slower to compute. We tested $\eta = \{0.1, 0.2, 0.3\}$ and settled on $0.2$.  

- *gamma* -- Given some of the trees we tested were relatively deep, we experimented with a wide range of gamma. We used high gammas (i.e., $\{1, 2\}$) for models with deeper trees and (more common) lower gammas (i.e., $\{0, 0.01, 0.1\}$) for shallower ones. Our best model used the default gamma = $0$.  

- *max depth* -- Given the large number of parameters, we first experimented with relatively deep trees, setting max depth to 8, 9, and 10. However, we accomplished optimal performance with max_depth = $7$, implying the existence of 7-way interactions of covariates.  

- *Fraction of observations to be randomly samples for each tree* (i.e., `subsample`) -- Lower values make the algorithm more conservative and prevents overfitting but making it too small might lead to under-fitting. Given the size of the data set, this parameter had little effect on performance. We settled on the default value of $0.5$.  

- *Fraction of columns to be randomly sampled for each tree* (i.e., `colsample_bytree`) -- As was revealed in EDA, a few covariates are very highly correlated to response. We anticipated the algorithm prefer such covariates. This may prevent us from discovering other covariates with significant predictive power if we keep the default `colsample_bytree` = $1$. However, tinkering with values in $[0.1, 1]$ revealed that no such latent variables exist.

- *nrounds* -- We found that running the algorithm for more than $3,000 - 10,000$ rounds led to overfitting.  

[9] https://www.quora.com/What-does-the-threshold-value-in-the-neuralnet-function-represent



# 5 Data Splits

## Neural Network

&nbsp;&nbsp;&nbsp;&nbsp; Since `neuralnet` did not have an out-of-the-box cross-validation function, we implemented one (see below). The algorithm below implements 10-fold cross validation on our optimal network structure (i.e., single hidden layer with 10 units). We run each fold 5 times and store the mean error and weights.  

```{r, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4, eval = FALSE}

NN.cv <- function(X_std_w.loss, k = 10, structure = 10, thresh = 20){
  
  sum_NN_perFold <- rep(list(list(NN = NA, weights = NA, error = NA)), k)

  # k-fold cross-validation
  k.folds.indicators <- cut(seq(1, nrow(X_std_w.loss)), breaks = k, labels=FALSE)
  rNums_shuf         <- sample(nrow(X_std_w.loss))
  X_std_w.loss_shuf  <- X_std_w.loss[rNums_shuf,]
  
  formula_m.NN <- as.formula(paste("loss ~ ", 
                                   paste(importance_matrix_sub$Feature, collapse = " + "), 
                                   collapse = " "))
  
  error   <- numeric(k)
  NNs     <- rep(list(list()), k)
  weights <- matrix(nrow = )
  
  for(i in 1:k){
  
    indx_test  <- which(k.folds.indicators == i, arr.ind=TRUE)
  
    # Run NN on ith fold
    X_std_w.loss_shuf_k.fold.train <- X_std_w.loss_shuf[-indx_test, ]
  
    NN_ith.kfold <- neuralnet(formula_m.NN, data = X_std_w.loss_shuf_k.fold.train, 
                              hidden = structure, rep = 5, act.fct = "tanh", 
                              lifesign = "full", threshold = thresh, linear.output = T)
  
    # Compute rmse for ith fold
    X_std_w.loss_shuf_k.fold.test <- X_std_w.loss_shuf[indx_test, ]
    predictions_std <- compute(NN_ith.kfold, 
                               X_std_w.loss_shuf_k.fold.test[ , names(X_std_w.loss_shuf_k.fold.test) != "loss"] )
    predictions_std <- unscale(predictions_std$net.result, loss_scaled)
    
    error[i] <- rmse(predictions_std, X_std_w.loss_shuf_k.fold.test$loss)
    NNs[[i]] <- NN_ith.kfold
    weigths
    
    dists <- t(apply(X, 1, dists.to.point, mu))
    b <- apply(dists, 1, which.min)
    
    images_train_shuf_k.fold.test  <- images_train_shuf[indx_test, ]
    labels_train_shuf_k.fold.test  <- labels_train_shuf[indx_test]
  
    sum_NN_perFold[[i]]$weights <- mean(NN_ith.kfold$result.matrix$weights)
    sum_NN_perFold[[i]]$error   <- mean(NN_ith.kfold$result.matrix$error)
  
  }
  
  weights <- rep(0, dim(X_std_w.loss)[2])
  error   <- numeric()
  for(i in 1:k){
    weights <- weights + sum_NN_perFold[[i]]$weights
    error   <- error   + sum_NN_perFold[[i]]$error
  }
  
  weights_avg <- weights / k
  error_avg   <- error / k
  
  return(list(weights = weights_avg, error = error_avg))
  
}


```


## XGboost

&nbsp;&nbsp;&nbsp;&nbsp; XGBoost provides a cross-validation `xgb.cv`. We set `nfold` equal to 10 and used the parameters specified in "Hyperparameter Selection."  


# 6 Errors and Mistakes

&nbsp;&nbsp;&nbsp;&nbsp; Finding methods to tease out relationships in the data was particularly time-consuming. Preliminary XGBoost proved useful but not comprehensive. Our primary objectives were to  

- (1) eliminate extraneous variables;
- (2) reduce multicollinarity and concurvity -- crucial considerations general additive models, among others --; and,
- (3) and illustrate the main relationships.  

To this end, we attempted to calculate VIF scores for the covariates, but the `vif` function would only return `NaN`. We then considered kmeans but soon realized this did not provide a robust method to summarize the main effects in the data. Next, we ran principal component analysis (PCA). Although this proved particularly enlightening, the fact that it does not factor in the response `loss` was a major shortcoming. Only then did we discover and implement partial least squares. In future, we would run some form of hierarchical clustering or k-nearest neighbors as a non-linear equivalent to PLS.  

&nbsp;&nbsp;&nbsp;&nbsp; Additionally, we should practice a "fail fast, fail often" approach. That is, experimental test runs should take a few minutes, thus allowing us to quickly discard particularly poor-performing approaches. We can typically do this by reducing the size of the data set or the number of iterations.  

# 7 Predictive Accuracy

## Neural Network

&nbsp;&nbsp;&nbsp;&nbsp; The best-performing neural net model scored $MAE = 1307.3$. As alluded to in previous sections, it was the product of 10-fold cross-validation under following parameters:  

- `hidden` (i,e., hidden layers and neurons per layer): $10$
- `threshold`: $20$. Notice this is significantly lower than the threshold we would've used on the entire data set given cross-validation only uses a tenth of the data.
- `stepmax`: $100,000$
- `rep` (i.e., repetitions): $5$
- `algorithm`: `rprop+`
- `act.fct` (i.e., activation function): `tanh`  

\vspace{10pt}

```{r, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4, eval = FALSE}

NN_mod <- NN.cv(X_std_w.loss, structure = 10, thresh = 20)

# X_test_std_NN <- X_test_std[,importance_matrix_sub$Feature]
# predictions_std <- compute(NN, X_test_std_NN)
# submission$loss <- unscale(predictions_std$net.result, loss_scaled)
# write.csv(submission, file = "submission.csv", row.names = FALSE)

```

## XGboost

&nbsp;&nbsp;&nbsp;&nbsp; The best-performing XGboost model scored $MAE = 1278.6$.  As alluded to in previous sections, it was the product of 10-fold cross-validation under the following parameters:  

- `eta` (i.e., learning rate): $0.2$
- `gamma`: $0$
- `max_depth`: $7$
- `subsample`: $0.5$
- `colsample_bytree`: $1$
- `nrounds`: $2000$  

\vspace{10pt}

```{r, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4, eval = FALSE}

xgboost_mod <- xgb.cv(data = X, label = train$loss, 
                      params = list(eta = 0.2, gamma = 0, max_depth = 7, subsample = 0.5, 
                                    colsample_bytree = 1), nrounds = 2000, nfold = 10)


# predictions <- predict(xg_output, as.matrix(X_test))
# 
# submission$loss <- predictions
# write.csv(submission, file = "submission.csv", row.names = FALSE)

```
