---
title: "Kaggle Competition"
author: "Ricardo Batista (rb313)"
date: "4/18/2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(knitr)
library(tictoc)
library(car)
library(glmnet)
library(xgboost)
library(HH)
library(GGally)
library(neuralnet)
library(Radviz)
library(DiagrammeR)
library(BBmisc)

set.seed(2018)

train_raw      <- read.csv("pml_train.csv", header = TRUE)
test_raw       <- read.csv("pml_test_features.csv", header = TRUE)
submission_raw <- read.csv("pml_example_submission.csv", header = TRUE)


#### Data cleaning ####
train          <- dplyr::select(train_raw, -id)
test           <- dplyr::select(test_raw, -id)
submission     <- submission_raw


# sum(apply(train, 2, is.na))
# sum(apply(train, 2, is.nan))
# sum(apply(train, 2, is.infinite))


X_raw      <- model.matrix(loss ~ - 1 + . , train)
X_test_raw <- model.matrix( ~ - 1 + . , test)

cov.intersect <- intersect(colnames(X_raw), colnames(X_test_raw))
X_w.alias      <- X_raw[ , cov.intersect]
X_test_w.alias <- X_test_raw[ , cov.intersect]


# Remove the aliases
### Consider doing the partial alias stuff in lm
X_w.alias_lm      <- data.frame(cbind(X_w.alias, loss = train$loss))

# make live
# tic("linear model")
# m.lm_w.alias <- lm(loss ~ ., X_w.alias_lm)
# toc()

# make live
# aliases  <- rownames(alias(m.lm_w.alias)$Complete)

# delete
aliases  <- c("cat1B", "cat74C", "cat81D", "cat85D", "cat87D", "cat90F", "cat91H", "cat92I", "cat98D", "cat99F", "cat99M", "cat99R", "cat99T", "cat100E", "cat103K", "cat103L", "cat106R", "cat107R", "cat107U", "cat108F", "cat108J", "cat111W", "cat113BI", "cat115B", "cat115D", "cat115W", "cat115X", "cat116AF", "cat116MA", "cat116MC", "cat116MO", "cat116MP", "cat116MW", "cat116X")


X_w.coll_lm   <- dplyr::select(X_w.alias_lm, -one_of(aliases))
X_test_w.coll <- dplyr::select(as.data.frame(X_test_w.alias), -one_of(aliases))

# Ancillary objects
cont.var <- grep("cont", colnames(train), value=T)
cat.var  <- grep("cat", colnames(train), value=T)

```

# Exploratory Analysis

&nbsp;&nbsp;&nbsp;&nbsp; The glaring issue with EDA on this data set is its high dimensionality. First, we narrow down the covariate space by using XGBoost and LASSO on the full data set. Next, we perform kmeans on the "important" covariates to tease out potential interactions. Finally, we project this high-dimensional covariate space into two dimensions to glean additional insight.  

## XGBoost

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

#### Boosting ####

# xgb.DMatrix
X_w.coll_xg <- as.matrix(dplyr::select(X_w.coll_lm, -loss))
xg_output <- xgboost(data = X_w.coll_xg, label = train$loss, nrounds = 100)

## Results ##
# [2999]	train-rmse:600.058289 
# [3000]	train-rmse:599.908325 


# Variable importance
importance_matrix <- xgb.importance(colnames(X_w.coll_xg), model = xg_output)
xgb.plot.importance(importance_matrix, rel_to_first = TRUE, xlab = "Relative importance")

# Variable subset (arbitrarily chose 0.5%)
importance_matrix_sub <- importance_matrix %>%
  dplyr::select(c(Feature, Importance)) %>%
  filter(Importance > 0.005)

kable(importance_matrix_sub)

```

&nbsp;&nbsp;&nbsp;&nbsp;  As the XGBoost variable importance plot suggests, only a few variables are highly correlated to loss. The table shows the subset of covariates with variable importance above 0.05%.  

## LASSO

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Lasso
tic("lasso (cross-validated)")
X_w.coll <- dplyr::select(X_w.coll_lm, -loss)
m.lasso  <- cv.glmnet(as.matrix(X_w.coll), X_w.coll_lm$loss)
toc()

predictions <- predict.cv.glmnet(m.lasso, X_test)

#### Ancillary operations ####

# Retrieve covariate names
bestlam    <- m.lasso$lambda.min
lasso.coef <- predict(m.lasso, type="coefficients", s = bestlam)
cov.lasso  <- colnames(X_w.coll)[(lasso.coef@i)[-1]]

```

&nbsp;&nbsp;&nbsp;&nbsp;  The table above shows the subset of covariates with non-zero coefficients. Notice the overlap with XGBoost. From this point forward, we'll focus on the union of the two sets of covariates.  

&nbsp;&nbsp;&nbsp;&nbsp;  Now that we've narrowed down the covariate space, we perform kmeans to identify potential interactions.  

## k-means

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# kmeans
## Given sqrt(dim(train)[1]) = 363.073, we go for 300

# Delete
importance_matrix_sub_features <- c("cat80B", "cont7", "cat79D", "cat57B", "cont2", "cat80D", "cat12B", "cont12", "cont14", "cat87B", "cat81B", "cat72B", "cat1A", "cat113U", "cat53B", "cat10B", "cont11", "cont3", "cat100I", "cat44B", "cont1", "cat2B", "cat4B")

# replace
X_kmeans_lm <- dplyr::select(X_w.coll_lm, c(importance_matrix_sub_features, "loss"))
# X_kmeans_lm <- dplyr::select(X_w.coll_lm, c(importance_matrix_sub$Feature, "loss"))

k <- seq(2, sqrt(dim(train)[1]), 30)
kmeans_cost <- data.frame(k = k, WCSS = NA) # within-cluster sum-of-squares
for (i in 1:length(k)){
  
  gc()
  kmeans_output <- kmeans(X_kmeans_lm, algorithm = "Lloyd", centers = k[i], trace = T)
  # Alternative parameters:
  ## nstart = 25
  ## iter.max = 20
  ## More iterations {20, 50}
  ## algorithms = {"MacQueen" "Lloyd"}
  ## some nstar greater
  
  kmeans_cost[i, 2] <- kmeans_output$tot.withinss
  
  # Verbose
  print(k[i])
  
}

# Looking for diminishing returns
#kmeans_cost$totss <- log10(kmeans_cost$totss)
ggplot(kmeans_cost, aes(k, WCSS)) + geom_point() + geom_line()

```

&nbsp;&nbsp;&nbsp;&nbsp;  The point of diminishing returns is clearly at $k \sim $  Finally, as a sanity check, we project this subset of variables into a 2D space.

## 2D projection

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Radviz requires that all dimensions have the same range. 
# The simplest way to achieve that is to normalize every numerical column to its range, so that all values fall in the [0,1] interval. 
norm <- X_kmeans_lm
norm <- norm[,colnames(X_raw)[colnames(X_raw) %in% colnames(X_kmeans_lm)]]
cont.var_xg <- grep("cont", colnames(norm), value=T)
norm[, c(cont.var_xg)] <- apply(norm[, c(cont.var_xg)], 2, do.L, fun=function(x) quantile(x,c(0.025,0.975)))

# The make.S function will take a vector of column names and return a matrix of [x,y] positions. 
# The order of the column names will dictate the position on the circle, starting at 12:00 and going clockwise.
ct.S <- make.S(colnames(norm))

## compute the similarity matrix
ct.sim <- cosine(as.matrix(norm))
## the current radviz-independent measure of projection efficiency
in.da(ct.S,ct.sim)

## the current radviz-independent measure of projection efficiency
rv.da(ct.S,ct.sim)

# The optimization procedure works as follows:
optim.ct <- do.optim(ct.S,ct.sim,iter=100,n=1000)
ct.S <- make.S(tail(optim.ct$best,1)[[1]])

# The do.radviz function will then use the normalized values and the Springs to project each thing in a 2D space:
ct.rv <- do.radviz(norm,ct.S)

# Visualizing the results
loss_norm <- do.L(train$loss, fun = function(x) quantile(x,c(0.025,0.975)))
plot(ct.rv, point.color = rgb(0, 0, loss_norm, 0.5))

```

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

#Continuous

# Radviz requires that all dimensions have the same range. 
# The simplest way to achieve that is to normalize every numerical column to its range, so that all values fall in the [0,1] interval. 
norm <- X_kmeans_lm
norm <- norm[,colnames(X_raw)[colnames(X_raw) %in% colnames(X_kmeans_lm)]]
norm <- norm[, c(cont.var_xg)]

# The make.S function will take a vector of column names and return a matrix of [x,y] positions. 
# The order of the column names will dictate the position on the circle, starting at 12:00 and going clockwise.
ct.S <- make.S(colnames(norm))

## compute the similarity matrix
ct.sim <- cosine(as.matrix(norm))
## the current radviz-independent measure of projection efficiency
in.da(ct.S,ct.sim)

## the current radviz-independent measure of projection efficiency
rv.da(ct.S,ct.sim)

# The optimization procedure works as follows:
optim.ct <- do.optim(ct.S,ct.sim,iter=100,n=1000)
ct.S <- make.S(tail(optim.ct$best,1)[[1]])

# The do.radviz function will then use the normalized values and the Springs to project each thing in a 2D space:
ct.rv <- do.radviz(norm,ct.S)

# Visualizing the results
loss_norm <- do.L(train$loss, fun = function(x) quantile(x,c(0.025,0.975)))
plot(ct.rv, point.color = rgb(0, 0, loss_norm, 0.5))

```

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Categorical

# Radviz requires that all dimensions have the same range. 
# The simplest way to achieve that is to normalize every numerical column to its range, so that all values fall in the [0,1] interval. 
norm <- X_kmeans_lm
norm <- norm[,colnames(X_raw)[colnames(X_raw) %in% colnames(X_kmeans_lm)]]
cat.var_xg <- grep("cat", colnames(norm), value=T)
norm <- norm[, c(cat.var_xg)]

# The make.S function will take a vector of column names and return a matrix of [x,y] positions. 
# The order of the column names will dictate the position on the circle, starting at 12:00 and going clockwise.
ct.S <- make.S(colnames(norm))

## compute the similarity matrix
ct.sim <- cosine(as.matrix(norm))
## the current radviz-independent measure of projection efficiency
in.da(ct.S,ct.sim)

## the current radviz-independent measure of projection efficiency
rv.da(ct.S,ct.sim)

# The optimization procedure works as follows:
optim.ct <- do.optim(ct.S,ct.sim,iter=100,n=1000)
ct.S <- make.S(tail(optim.ct$best,1)[[1]])

# The do.radviz function will then use the normalized values and the Springs to project each thing in a 2D space:
ct.rv <- do.radviz(norm,ct.S)

# Visualizing the results
loss_norm <- do.L(train$loss, fun = function(x) quantile(x,c(0.025,0.975)))
plot(ct.rv, point.shape=1, point.color = rgb(0, 0, loss_norm))
smoothRadviz(ct.rv)

```



# Model 1: Boosting (XGBoost)

&nbsp;&nbsp;&nbsp;&nbsp;  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

#### Boosting ####

X_boost_int <- as.matrix(dplyr::select(X_w.coll_lm, c(importance_matrix_sub$Feature)))
xg_output <- xgboost(data = X_boost_int, label = train$loss, nrounds = 300)
# Parameters
## nrounds = 3000

# [2999]	train-rmse:764.766907 
# [3000]	train-rmse:764.469177

# xgb.plot.tree(feature_names = names(X_boost_int), model = xg_output)

# Interactions

featureList <- importance_matrix_sub$Feature
featureVector <- c() 
for (i in 1:length(featureList)) {
  featureVector[i] <- paste(i-1, featureList[i], "q", sep="\t")
}
write.table(featureVector, "fmap.txt", row.names=FALSE, quote = FALSE, col.names = FALSE)
xgb.dump(model = xg_output, fname = 'xgb.dump', fmap = "fmap.txt", with.stats = TRUE)


```


## Training

&nbsp;&nbsp;&nbsp;&nbsp;  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}


```

## Hyperparameter Selection

&nbsp;&nbsp;&nbsp;&nbsp;  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}


```

## Predictive Accuracy

&nbsp;&nbsp;&nbsp;&nbsp;  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}


```



# Model 2: Neural Network

&nbsp;&nbsp;&nbsp;&nbsp;  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

#### Neural Network ####

# Normalize data
X_NN_lm <- X_kmeans_lm
max <- apply(X_NN_lm, 2, max)
min <- apply(X_NN_lm, 2, min)
X_scaled <- as.data.frame(scale(X_NN_lm, center = min, scale = max - min))

# fit neural network
formula_m.NN <- as.formula(paste("loss ~ ", paste(importance_matrix_sub$Feature, collapse = " + "), collapse = " "))
NN <- neuralnet(formula_m.NN, data = X_scaled, hidden = 3 , linear.output = T, lifesign = "full")


# plot neural network
plot(NN)

X_test_NN <- X_test_w.coll[,importance_matrix_sub$Feature]
predict_testNN <- compute(NN, X_test_NN)
predict_testNN <- (predict_testNN$net.result * (max(train$loss) - min(train$loss))) + min(train$loss)

```




## Training

&nbsp;&nbsp;&nbsp;&nbsp;  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}


```

## Hyperparameter Selection

&nbsp;&nbsp;&nbsp;&nbsp;  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}


```

## Predictive Accuracy

&nbsp;&nbsp;&nbsp;&nbsp; 

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Stepwise selection #
lm_full_step <- step(lm_full, trace = 0, direction = c("both"))
```





```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Continuous data
cont.var <- grep("cont", colnames(train), value=T)
ggpairs(train, columns = c(cont.var[1:7], "loss"))
ggpairs(train, columns = c(cont.var[8:14], "loss"))


# Categorical
p <- ggplot(train, aes(x = cat80, y = loss)) + geom_boxplot()

ylim1 = boxplot.stats(train$loss)$stats[c(1, 5)]

# scale y limits based on ylim1
p1 = p + coord_cartesian(ylim = ylim1*1.05)



p <- ggplot(train, aes(y = loss)) + geom_boxplot()
# With one variable
p + facet_grid(. ~ cat23)


boxplot <- ggplot(data = train) + 
   geom_boxplot(aes( x = sex, y = loss), position = 'identity') + 
   facet_grid(rank ~ discipline)
> boxplot


```

# Data Splits

&nbsp;&nbsp;&nbsp;&nbsp;  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}


```





# Conclusion

## Errors and Mistakes



<!-- Reference predictive accuracy -->


---
Shipyard



```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Full linear model
lm_full <- lm(loss ~ ., train)

# Stepwise selection #
lm_full_step <- step(lm_full, trace = 0, direction = c("both"))
```


```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Lasso
grid <- 10^(seq(10,-2, length=100))

m.lasso <- glmnet(X, train$loss, alpha=1, lambda=grid)
bestlam <- m_lasso$lambda.min
lasso.coef <- predict(m_lasso, type="coefficients", s = bestlam)

preds_lasso <- colnames(X)[(lasso.coef@i)[-1]]
exp_m.lasso <- as.formula(paste("loss ~ ", paste(preds_lasso, collapse = " + "), collapse = " "))

m_lm <- lm(exp_m.lasso, X)

# save(predictions, file="../predict-test.Rdata")

# Check collinearity

```


```{r , echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

#### VIF ####

tic("linear model")
m.lm_w.coll <- lm(loss ~ ., X_w.coll_lm)
toc()

tic("vif")
vif_output <- vif(m.lm_w.coll)
toc()


tic("linear model")
m.lm_w.coll <- lm(loss ~ ., X_w.coll_lm, x=TRUE)
toc()

tic("vif")
vif_HH_output <- HH::vif(m.lm_w.coll)
toc()

# vif_custom <- function(X, thresh = 10){
#
#  }

```


```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

#### Boosting ####

# xgb.DMatrix
X_w.coll_xg <- as.matrix(dplyr::select(X_w.coll_lm, -loss))
xg_output <- xgboost(data = X_w.coll_xg, label = train$loss, nrounds = 3000)

## Results ##
# [2999]	train-rmse:600.058289 
# [3000]	train-rmse:599.908325 


# Variable importance
importance_matrix <- xgb.importance(colnames(X_w.coll_xg), model = xg_output)
xgb.plot.importance(importance_matrix, rel_to_first = TRUE, xlab = "Relative importance")

# Use some cross-validation / tinker with parameters


# Predictions
predictions <- predict(xg_output, as.matrix(X_test_w.coll))

submission$loss <- predictions
write.csv(submission, file = "submission.csv", row.names = FALSE)
```



```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 5, fig.height = 4}

# Lasso
tic("lasso (cross-validated)")
X_w.coll   <- dplyr::select(X_w.coll_lm, -loss)
m.lasso <- cv.glmnet(X, X_w.coll_lm$loss)
toc()

predictions <- predict.cv.glmnet(m.lasso, X_test)

submission$loss <- predictions
save(submission, file="submission.csv")


#### Ancillary operations ####

# Retrieve covariate names
bestlam    <- m.lasso$lambda.min
lasso.coef <- predict(m_lasso, type="coefficients", s = bestlam)
cov.lasso <- colnames(X)[(lasso.coef@i)[-1]]

```

